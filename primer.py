import argparse
import primer3
from Bio.Seq import Seq

args = {}

def temp_USER(seq):
    '''Returns melting temperature in the context of a USER junction'''
    # these values don't quite match IDT's oligoanlyzer values; there's not enough info
    # on IDT's page to replicate their calculation.
    return primer3.calcTm(seq.upper(), dna_conc=250, mv_conc=50, dv_conc=10, dntp_conc=0)

def temp_PCR(seq):
    '''Returns melting temperature in the context of PCR'''
    return primer3.calcTm(seq.upper(), dna_conc=250, mv_conc=50, dv_conc=7.5, dntp_conc=0.25)

def hairpin(seq):
    '''Returns Tm of hairpin with highest melting temperature'''
    if len(seq) > 60 or len(seq) < 5:       # primer 3 can't handle calculating >60bp
        # TODO logging
        return None
    h = primer3.calcHairpinTm(seq.upper(), dna_conc=250, mv_conc=50, dv_conc=7.5, dntp_conc=0.25)
    # TODO logging
    return h

class primer:
    ''' Contains all information about a single primer, including identifying its annealing
    
        portion etc. so that it can be formatted when printed
    ''' 
    def __init__(self, s, b, j, m = []):
        self.seq = s
        self.hairpin = hairpin(s)
        self.binding_len = b
        self.junction_len = j
        self.mod_len = m
        
    def __repr__(self):
        return self.seq
        
    def len(self):
        return len(self.seq)
    
    def valid(self):
        ''' Returns true if primer meets minimum requirements such as length '''
        if self.seq is not None and self.len() < args['max_primer_length']:
            return True
        return False
        
    def HTTP_primer(self):
        '''Prints the primer, with uppercase USER junction, red modifications, underlined
        annealing portion'''
        global out
        
        # TODO asserts
        out += '<font face="Courier">'
        formatted = []
        for i in range(len(self.seq)):
            base = self.seq[i].lower()
        
            # reformat the uracils
            if self.junction_len and i == self.junction_len-1:
                base = args['uracil_character']
            
            # USER junction in caps
            if self.junction_len and i < self.junction_len:
                base = base.upper()
            
            # binding portion in italics
            if self.binding_len and i >= len(self.seq) - self.binding_len - 1:  # TODO not sure why there's a -1 here
                base = "<u>" + base + "</u>"
       
            # modified bases are red
            if self.mod_len and self.binding_len:
                binding_end = len(self.seq) - self.binding_len - 1
                if i in range (binding_end - self.mod_len, binding_end):
                    base = '<font color="blue">' + base + "</font>"
        
            formatted += base
        out += "".join(formatted)
        out += "</font><br>"

    def HTTP_hairpin_temp(self):
        global out
        if self.hairpin >= 55:
            out += '<font color="red">'
        elif self.hairpin >= 40:
            out += '<font color="orange">'
        elif self.hairpin < 40:
            out += '<font color="green">'
            
        if self.hairpin is None:
            out += "Hairpin not calculated</font>"
        else:
            s =' %.2f </font>' % (self.hairpin)
            out += s
        
class primer_pair:
    ''' Tracks a primer pair that binds at a particular USER site '''
    def __init__(self, user_site):
        self.F = None
        self.R = None
        self.user_site = user_site
        self.bp_removed_left = 0
        self.bp_removed_right = 0
        
    def __repr__(self):
        return "Junction: " + self.user_site + " F: " + str(self.F) + " R: " + str(self.R)
        
    def set_F(self, F):
        self.F = F
        
    def set_R(self, R):
        self.R = R
        
    def set_removed_bases(self, l, r):
        self.bp_removed_left = l
        self.bp_removed_right = r

    def max_hairpin_Tm(self):
        return max(self.F.hairpin, self.R.hairpin)
        
    def heterodimer_Tm(self):
        primer_dimer = primer3.calcHeterodimer(self.F.seq, self.R.seq)
        return primer_dimer.tm
        
    def is_valid(self):
        ''' Returns true if primer pair meets minimum requirements such as length and
            sequence '''
        if self.R.valid() and self.F.valid() and self.max_hairpin_Tm() < args['max_hairpin_tm']:
            return True
        return False

class all_primer_solutions:
    ''' Accumulates all primer solutions and can print them, sorted and formatted '''
    def __init__(self):
        self.results = []
    
    def add(self, x):
        self.results.append(x)
        
    def HTTP_formatted_primer_options(self, ignore_filters = 0):
        '''Prints the primers in HTTP format'''
        global out
        
        printed = 0
        # sort by max hairpin temp
        for x in sorted(self.results, key=lambda x: x.max_hairpin_Tm()):
            if x.is_valid() or ignore_filters:
                printed = 1
                s = '\n<p>For USER junction Tm: %.2f ' % (temp_USER(x.user_site))
                out += s
                out += x.user_site.upper() + '<br>'
                
                s = "Heterodimer Tm: %.2f<br>" % x.heterodimer_Tm()
                out += s
                if x.bp_removed_left != 0 or x.bp_removed_right !=0:
                    s = "Removed %d bp from left and %d bp from right<br>" % (x.bp_removed_left,x.bp_removed_right)
                    out += s
        
                out += 'Primer 1: max hairpin:'
                x.F.HTTP_hairpin_temp()
                x.F.HTTP_primer()
        
                out += 'Primer 2: max hairpin:'
                x.R.HTTP_hairpin_temp()
                x.R.HTTP_primer()

        if printed == 0 and ignore_filters == 0:
            out += '<p><font color="red"><b>No acceptable primer solutions found. Here are some bad options:</b></font><p>'
            self.HTTP_formatted_primer_options(ignore_filters = 1)
   
def find_USER_junctions(seq, output=False):
    '''Identifies all USER junctions in the given sequence that obey length and Tm requirements'''
    seq = seq.upper()
    junctions = {}
    for i in range(len(seq)):
        if seq[i] == 'A':
            for j in range(i, min(i+args['max_user_length'], len(seq))):
                if seq[j] == 'T':
                    possible_junction = seq[i:j+1]
                    Tm = temp_USER(possible_junction)
                    if Tm > args['min_user_tm'] and Tm < args['max_user_tm']:
                        junctions[(i, j+1)] = Tm
                        if output:
                            print seq
                            print ' '*i + seq[i:j+1] + ' '*(len(seq)-j), '\t',
                            print "%.1f" % Tm
    return junctions

def annealing_part(seq, five_prime, direction):
    '''Returns the sequence of a primer that beings at the specified five_prime location,
    points either 'F' or 'R', and anneals to the sequence with annealing_portion_tm '''
    temp = 0
    length = 5
    primer_sequence = ''
    while temp < args['annealing_portion_tm']:
        length = length + 1
        if length > len(seq):
            return None
        if direction == 'F':
            primer_sequence = seq[five_prime : five_prime + length]
        else:
            primer_sequence = seq[five_prime - length : five_prime]
        temp = temp_PCR(primer_sequence)
    return primer_sequence

def calculate_primer_sequences_for_mod(results, s, S, E):
    '''Inputs:
    - sequence s
    - the start and end indices, S and E,  of the desired modification.

    Returns all sets of F, R primers where the USER site is to the RIGHT of the desired
    modification.

    Example. Annealing portions *starred*
    ggatggcatgacagtaagagaattatgtTAGAgccgccataaccatgagtgataacact
                                        ---USER----
    R primer     <**************TAGA---------------
    F primer                            *********************>
    '''
    # find all USER junctions
    junctions = find_USER_junctions(s, output=False)

    # Find the annealing portion of the R primer
    annealing_part_primer = annealing_part(s, S-1, 'R')

    # This primer can be paired with any USER site to the right of the modified region
    front_junctions = [(i, j) for (i, j) in junctions.keys() if i >= E]
    for (i, j) in front_junctions:
        # new result for this USER junction
        result = primer_pair(s[i:j])
        
        # Find the entirety of the Forward primer. This is easy: the whole thing anneals
        # and it starts with the USER site
        f_primer = annealing_part(s, i, 'F')
        if f_primer is None:
            continue
        pF = primer(f_primer, len(f_primer), j-i)     # the entire primer binds. The junction is the length of the USER junction
        result.set_F(pF)
        
        # Extend the R primer so it covers the USER site
        r_primer = s[S-1-len(annealing_part_primer):j]
        pR = primer(str(Seq(r_primer).reverse_complement()), len(annealing_part_primer), j-i, m =E-S)    
        result.set_R(pR)
        
        results.add(result)
    
def calculate_primer_sequences_for_mod_wrapper(results, s):
    # identify the modified region, indicated with CAPS
    caps = [i for i in range(len(s)) if s[i].isupper()]

    # calculate acceptable primer sequences
    calculate_primer_sequences_for_mod(results, s, caps[0], caps[-1]+1)

def find_USER_primers_for_mod(s):
    # given a sequence, detect where the mod is (in CAPS), and calculate USER primers,
    # where the USER junction is either to the left or right of the mod.
    global out
    out += "Finding primers to introduce modification<p>"
    out += "Target sequence: " + s
    
    results = all_primer_solutions()
    
    calculate_primer_sequences_for_mod_wrapper(results, s)

    calculate_primer_sequences_for_mod_wrapper(results, str(Seq(s).reverse_complement()))
    
    results.HTTP_formatted_primer_options()

def calculate_primer_sequences_for_junction(left, right):
    '''
    Calculate all ways to join the left and right sequence, where the left primer fully
    anneals because it is to the LEFT of the USER site.
    '''
    
    results = all_primer_solutions()
    
    # find all USER junctions
    s = left+right
    junctions = find_USER_junctions(s, output=False)
    
    # all junctions in which the lefthand primer binds completely
    front_junctions = [(i, j) for (i, j) in junctions.keys() if j <= len(left)]
    for(i, j) in front_junctions:
        result = primer_pair(s[i:j]) 
        
        # Find the entirety of the reverse primer. This is easy: the whole thing anneals
        r_primer = annealing_part(s, j, 'R')
        if r_primer is None:
            continue
        pR = primer(str(Seq(r_primer).reverse_complement()), len(r_primer), j-i)     # the entire primer binds. The junction is the length of the USER junction
        result.set_R(pR)
        
        # Find the annealing portion of the F primer
        start_anneal = max(j, len(left)+1)
        annealing_part_primer = annealing_part(s, start_anneal, 'F')
        if annealing_part_primer is None:
            continue
        # Extend the F primer so it covers the USER site
        f_primer = s[i:start_anneal] + annealing_part_primer
        pF = primer(f_primer, len(annealing_part_primer), j-i)  
        result.set_F(pF)
        
        results.add(result)

    return results
    
def find_USER_junction(left, right):
    '''Identifies a USER junction that is near the junction between left and right'''

    r1 = calculate_primer_sequences_for_junction(left, right)
    
    r2 =calculate_primer_sequences_for_junction(str(Seq(right).reverse_complement()),\
                                                str(Seq(left).reverse_complement()))
                                                    
    # this is horrible TODO
    for x in r2.results:
        r1.add(x)
    
    return r1

def find_USER_junction_wrapper(left, right, optional_left_bp, optional_right_bp):
    global out
    out += "Joining two sequences with USER primers<p>"
    out +=  left + " " + right

    results = all_primer_solutions()
    
    results = find_USER_junction(left, right)
    for i in range(optional_left_bp):
        for j in range(optional_right_bp):
            new_results = find_USER_junction(left[:-i], right[j:])
            for r in new_results.results:
                r.set_removed_bases(i, j)
                results.add(r)

    results.HTTP_formatted_primer_options()
    
def get_parser():
    # Parse command line arguments
    parser = argparse.ArgumentParser(description='Generate USER primer options.')
    parser.add_argument('-i', "--input", type=str, required=False, help='the sequence you are\
                        trying to clone, with the modification in CAPS')
    parser.add_argument('-l', "--left", type=str, required=False, help='sequence to the\
                        left of the junction')
    parser.add_argument('-r', "--right", type=str, required=False, help='sequence to the\
                        right of the junction')
    parser.add_argument('-m', "--mode", type=str, required=False, help='script mode,\
                        either "modification", "junction"',\
                        default = "modification")
    parser.add_argument("--optional_left_bp", type=int, required=False, default=0,\
                        help='Number of optional bases on the left')
    parser.add_argument("--optional_right_bp", type=int, required=False, default=0,\
                        help='Number of optional bases on the right')
    parser.add_argument("--min_user_tm", type=float, required=False, default=35,\
                        help='Minimum Tm of the USER junction, default 35C')
    parser.add_argument("--max_user_tm", type=float, required=False, default=55,
                        help='Maximum Tm of the USER junction, default 55C')
    parser.add_argument("--max_user_length", type=float, required=False, default=15,
                        help='Maximum length of the USER junction, default 15bp')
    parser.add_argument('-Tm', "--annealing_portion_tm", type=float, required=False,\
                        default=65, help='Minimum temperature of the 3\' annealing portion of\
                        every primer, default 65C')
    parser.add_argument('-len', "--max_primer_length", type=float,required=False, default=60,\
                        help='Maximum primer length, default 60bp')
    parser.add_argument('-hairpin', "--max_hairpin_tm", type=float, required=False,\
                        default=65, help='Maximum allowed melting temperature for hairpins in\
                        primers, default 65C')
    parser.add_argument('-char', "--uracil_character", type=str, required=False,\
                        default='U', help='Character used to indicate uracils in primers. You\
                        probably want U, T or N.')     
    return parser

def main(args):
    if args["mode"] == 'modification':
        if args["input"] is not None:
            print args["input"]
            # TODO input sanitization
            find_USER_primers_for_mod(args["input"]) 
        else:
            print "<p>Provide input for modification mode<p>"
    
    elif args["mode"] == 'junction':
        if args["left"] is not None and args["right"] is not None:
            find_USER_junction_wrapper(args["left"], args["right"], args['optional_left_bp'], args['optional_right_bp'])
        else:
            print "<p>Provide both the sequence to the left and the right of the junction<p>"
    else:
        print "<p>Mode not recgonized<p>"   
   
if __name__ == '__main__':
    parser = get_parser()
    args = vars(parser.parse_args())
    main(args)
    
def mod_wrapper(input, min_user_tm, max_user_tm, max_user_length, max_primer_length, annealing_portion_tm, max_hairpin_tm, uracil_character):
    '''For non-command line calls'''    
    global out
    out = ''
    
    args['mode'] = 'modification'
    
    args['input'] = str(input)
    
    args['min_user_tm'] = min_user_tm
    args['max_user_tm'] = max_user_tm
    args['max_user_length'] = max_user_length
    args['max_primer_length'] = max_primer_length
    args['annealing_portion_tm'] = annealing_portion_tm
    args['max_hairpin_tm'] = max_hairpin_tm
    args['uracil_character'] = uracil_character
    

    main(args)

    return out  
  
def junction_wrapper(left, right, optional_left, optional_right, min_user_tm, max_user_tm, max_user_length, max_primer_length, annealing_portion_tm, max_hairpin_tm, uracil_character):
    '''For non-command line calls'''    
    global out
    out = ''
    
    args['mode'] = 'junction'
    
    args['left'] = str(left)
    args['right'] = str(right)
    args['optional_left_bp'] = optional_left
    args['optional_right_bp'] = optional_right
    
    args['min_user_tm'] = min_user_tm
    args['max_user_tm'] = max_user_tm
    args['max_user_length'] = max_user_length
    args['max_primer_length'] = max_primer_length
    args['annealing_portion_tm'] = annealing_portion_tm
    args['max_hairpin_tm'] = max_hairpin_tm
    args['uracil_character'] = uracil_character
    

    main(args)

    return out