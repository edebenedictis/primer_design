from flask import Flask, request, render_template
from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, SelectField, IntegerField, validators
import logging
from primer import mod_wrapper, junction_wrapper

app = Flask(__name__)

class basicform(FlaskForm):
    min_user_tm = IntegerField(u'Minimum USER junction Tm', validators=[validators.NumberRange(20,60)], default = 35)
    max_user_tm = IntegerField(u'Maximum USER junction Tm', validators=[validators.NumberRange(20,60)], default = 55)
    max_user_length = IntegerField(u'Maximum USER junction length', validators=[validators.NumberRange(5,20)], default = 15)
    max_primer_length = IntegerField(u'Max primer length', validators=[validators.NumberRange(20,60)], default = 60)
    annealing_portion_tm = IntegerField(u'Annealing Tm', validators=[validators.NumberRange(45,75)], default = 65)
    hairpin = IntegerField(u'Max primer hairpin Tm', validators=[validators.NumberRange(45,75)], default = 65)
    uracil_character = SelectField(u'Print uracils as', choices=[('U', 'U'), ('T', 'T'), ('N','N')], default = 'U')

class ModForm(basicform):
    input_sequence = TextAreaField(u'Input Sequence; mod in CAPS', validators=[validators.input_required()], default = 'ccgctgaaactgttgaaagttgtttagcaaaaCCCcatacagaaaattagatttactaacgtctggaaagacgacaaaactt')

class JunctionForm(basicform):
    left = TextAreaField(u'Left of junction sequence', validators=[validators.input_required()], default = 'ccgctgaaactgttgaaagttgtttagcaaaa')
    right = TextAreaField(u'Right of junction sequence', validators=[validators.input_required()], default = 'catacagaaaattagatttactaacgtctggaaagacgacaaaactt')
    
    optional_left = IntegerField(u'Optional left bp', validators=[validators.NumberRange(0,10)], default = 0)
    optional_right = IntegerField(u'Optional right bp', validators=[validators.NumberRange(0,10)], default = 0)
    
@app.route('/primer', methods=['GET', 'POST'], endpoint='primer')
def primer():
    logging.basicConfig(filename='primer.log',level=logging.DEBUG)
    input_string = ''

    if request.method == 'POST':
        form = ModForm(csrf_enabled=False)
        if form.validate_on_submit():
            try:
                return mod_wrapper(form.input_sequence.data,
                                   form.min_user_tm.data,
                                   form.max_user_tm.data,
                                   form.max_user_length.data,
                                   form.max_primer_length.data,
                                   form.annealing_portion_tm.data,
                                   form.hairpin.data,
                                   form.uracil_character.data)
            except:
                logging.exception("Error calling primer method" + str(form.data))
                return "An error has been logged."
            
        else:
            return "Error:<p>" + str(form.errors.items())
    else:
        form = ModForm(csrf_enabled=False)

    return render_template('create.html', form=form, title='USER primers for mutation',
                           instructions='Designs USER primers to introduce a mutation. Copy paste the desired sequence into the Input Sequence box, with any bases that differ from the template in CAPS.')    
              
@app.route('/junction', methods=['GET', 'POST'], endpoint='junction')
def junction():
    logging.basicConfig(filename='primer.log',level=logging.DEBUG)
    input_string = ''

    if request.method == 'POST':
        form = JunctionForm(csrf_enabled=False)
        if form.validate_on_submit():
            try:
                return junction_wrapper(form.left.data,
                                    form.right.data,
                                    form.optional_left.data,
                                    form.optional_right.data,
                                    form.min_user_tm.data,
                                    form.max_user_tm.data,
                                    form.max_user_length.data,
                                    form.max_primer_length.data,
                                    form.annealing_portion_tm.data,
                                    form.hairpin.data,
                                    form.uracil_character.data)
            except:
                logging.exception("Error calling junction method" + str(form.data))
                return "An error has been logged."
            
        else:
            return "Error:<p>" + str(form.errors.items())
    else:
        form = JunctionForm(csrf_enabled=False)

    return render_template('create.html', form=form, title='USER primers for Junction',
                           instructions='Designs USER primers to join together two fragments. Copy paste the two sequences into the left and right-of junction boxes. If the sequences around the junction aren\'t important, specify the number of optional bp on each side (warning: makes computation take much longer).')

if __name__ == '__main__':
    app.run(debug=True)